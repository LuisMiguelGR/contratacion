import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../servicios/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from './../../usuario';
import { usuarios } from '../../mock-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  visible = false;
  formuLogin: FormGroup;
  usuarioData: any;

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRouter: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loginService.login('peter@klaven', 'cityslicka').subscribe(res => {
      console.log(res);
      localStorage.setItem('actual', 'Luis Miguel');
    });
    this.formulario();
  }

logIn() {
  const usuario = this.formuLogin.get('usuario').value;
  const password = this.formuLogin.get('contrasena').value;
  this.loginService.login(usuario, password).subscribe(
    res => {
      console.log(res);
      let nuevoUsuario: Usuario = {username: usuario, password: password};
    },
    error => {
      console.error(error);
    },
    () => this.navegar()
  );
}

navegar() {
  this.router.navigateByUrl('/perfil');
}

  esVisible(): void {
    this.visible = !this.visible;
  }

  formulario() {
    this.formuLogin = this.formBuilder.group({
      usuario: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      contrasena: ['', [
        Validators.required,
        Validators.minLength(4)
      ]]
    });
  }

  onSubmit() {
    // this.usuarioData = this.saveUsuarioData();
    const valor = this.existe(this.usuarioData);
    if(valor){
      console.log(this.usuarioData);
      // this.loginService.inicioSesion(this.usuarioData);
    } else {
      alert('Usuario incorrecto');
    }
  }

  existe(usuario: Usuario): boolean {
    usuarios.forEach(element => {
      if (usuario.username === element.username) {
        console.log("existe");
        return true;
      }
    });
    return false;
  }

  // saveUsuarioData(): Usuario {
  //   const saveUsuario = {
  //     usuario: this.formuLogin.get('usuario').value,
  //     contrasena: this.formuLogin.get('contrasena').value
  //   }
  //   const usuarioNuevo = new Usuario(saveUsuario.usuario, saveUsuario.contrasena);    
  //   return usuarioNuevo;
  // }

  comprobar(): boolean {
    return this.loginService.estaOn();
  }



}
