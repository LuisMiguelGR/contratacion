import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Interviewed } from '../../interviewed';
import { Technology } from '../../technology';
import { TechnologiesService } from '../../servicios/technologies.service';
import { InterviewedService } from '../../servicios/interviewed.service';
import { MatSnackBar, MatSelect } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Provincias } from '../../provincias';
import { FirebaseStorageService } from '../../servicios/firebase.service';

@Component({
  selector: 'app-alta-entrevistado',
  templateUrl: './alta-entrevistado.component.html',
  styleUrls: ['./alta-entrevistado.component.scss']
})
export class AltaEntrevistadoComponent implements OnInit {

  nuevoInterviewed: Interviewed;
  tecnologias: Technology[];
  tecnologiasUsadas: Technology[];
  zoom = false;
  archivoSelec = null;
  formulario;
  caracteresObservaciones;
  provincias = Provincias;
  seleccionado: MatSelect;

  public mensajeArchivo = 'No hay cv seleccionado';
  public datosFormulario = new FormData();
  public nombreArchivo = '';
  public porcentaje = 0;
  public finalizado = false;
  public hayCv = false;

  constructor(
    private tecnologiaService: TechnologiesService,
    private interService: InterviewedService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private location: Location,
    private cd: ChangeDetectorRef,
    private firebaseService: FirebaseStorageService
  ) { }

  ngOnInit() {
    this.validarFormulario();
    this.tecUsadas();
    this.getTecnologias();
    this.nuevoInterviewed = new Interviewed();
  }

  public cambioArchivo(event) {
    if (event.target.files[0]) {
      this.hayCv = true;
      this.mensajeArchivo = `Nombre: ${event.target.files[0].name}`;
      this.nombreArchivo = event.target.files[0].name;
      this.datosFormulario.delete('cv');
      this.datosFormulario.append('cv', event.target.files[0], event.target.files[0].name);
    } else {
      this.mensajeArchivo = 'No hay cv seleccionado';
      this.hayCv = false;
    }
  }

  public subirArchivo() {
    let archivo = this.datosFormulario.get('cv');
    let tarea = this.firebaseService.tareaCloudStorage(this.nombreArchivo, archivo);
    tarea.then(() => {
      this.firebaseService.referenciaCloudStorage(this.nombreArchivo).then((value) => {
        this.nuevoInterviewed.cv = value;
      }).catch((error) => {
        console.error(error);
      });
    });
  }

  volver(): void {
    this.location.back();
  }

  tecUsadas(): void {
    this.interService.getTechnologiesUsadas().subscribe(techs => this.tecnologiasUsadas = techs);
  }

  addInterviewed(): void {

    this.nuevoInterviewed.tecnologias = this.tecnologiasUsadas;
    this.interService.addInterviewed(this.nuevoInterviewed).subscribe((value) => {
      this.snackBar.open(value, 'X', {
        duration: 3000
      });
    });
  }

  getTecnologias(): void {
    this.tecnologias = null;
    this.tecnologiaService.getTechnologies().subscribe(technologies => this.tecnologias = technologies);
  }

  updateTechnologiesUsadas(tech: Technology): void {
    this.interService.updateTechnologiesUsadas(tech).subscribe(techn => {

      this.tecnologiasUsadas = techn;
    });
  }

  addTecnologia(tecnologia: string): void {
    let idNew = 0;
    this.tecnologias.forEach(element => {
      if (tecnologia === element.name) {
        idNew = element.id;
      }
    });
    const tecnoMarcada = {
      id: idNew,
      name: tecnologia,
    };
    //this.tecnologiasUsadas.push(tecnoMarcada);
  }

  comprobarTecnologia(pTecnologia: string): boolean {
    console.log(pTecnologia);
    this.tecnologiasUsadas.forEach(element => {
      if (element.name === pTecnologia) {
        return true;
      }
    });
    return false;
  }

  getTecno() {
    console.log(this.tecnologiasUsadas);
  }


  
  // VALIDACIONES FORMULARIO

  validarFormulario(): any {
    this.formulario = this.formBuilder.group({
      dni: ['', [
        Validators.required,
        Validators.pattern('^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKETtrwagmyfpdxbnjzsqvhlcket]{1}$')
      ]],
      nombre: ['', [
        Validators.required,
        Validators.minLength(4)
      ]],
      apellidos: ['', [
        Validators.required,
        Validators.minLength(4)
      ]],
      direccion: ['', [
        Validators.maxLength(150)
      ]],
      email: ['', [
        Validators.email
      ]],
      telefono: ['', [
        Validators.required,
        Validators.pattern('^[9|6|7][0-9]{8}$')
      ]],
      fechaNacimiento: ['', [
        Validators.required
      ]],
      ciudad: [''],
      provincia: [],
      codigoPostal: ['', [
        Validators.pattern('^[0-5][1-9]{3}[0-9]$')
      ]],
      observaciones: ['', [
        Validators.maxLength(250)
      ]],
      cv: [null]
    });
  }

  submit() {
    if (this.formulario.valid) {
      // if (this.formulario.cv != null) {
      //   this.subirArchivo();
      // }
      this.subirArchivo();
      this.nuevoInterviewed = this.formulario.value;
      this.addInterviewed();
    } else {
      this.snackBar.open('No ha creado ningun entrevistado', 'X', {
        duration: 2000
      });
    }
  }

}


