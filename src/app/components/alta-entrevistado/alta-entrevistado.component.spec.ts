import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEntrevistadoComponent } from './alta-entrevistado.component';

describe('AltaEntrevistadoComponent', () => {
  let component: AltaEntrevistadoComponent;
  let fixture: ComponentFixture<AltaEntrevistadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEntrevistadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEntrevistadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
