import { Component, OnInit } from '@angular/core';
import { LogueadoService } from '../../servicios/logueado.service';
import { Usuario } from '../../usuario';
import { HttpBackend } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public registrado;
  estaOn = false;
  constructor(
    private loService: LogueadoService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.registrado = this.loService.getUsuarioLogueado();
    if (localStorage.getItem('actual') !== null) {
      this.estaOn = true;
      this.registrado = localStorage.getItem('actual');
    }
  }

  vaciarLocalStorage(): void {
    localStorage.removeItem('actual');
    localStorage.clear();
    this.router.navigateByUrl('/');
  }

}
