import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Interviewed } from '../../interviewed';
import { InterviewedService } from '../../servicios/interviewed.service';
import { ActivatedRoute } from '@angular/router';
import { TechnologiesService } from '../../servicios/technologies.service';
import { Technology } from '../../technology';
import { MatSnackBar } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Provincias } from '../../provincias';


@Component({
  selector: 'app-edit-interviewed',
  templateUrl: './edit-interviewed.component.html',
  styleUrls: ['./edit-interviewed.component.scss']
})
export class EditInterviewedComponent implements OnInit {

  public editInterviewed: Interviewed;
  public test: Interviewed = new Interviewed();
  tecnologias: Technology[];
  tecnologiasUsadas: Technology[];
  zoom = false;
  archivoSelec = null;
  fichero;
  provincias = Provincias;
  caracteresObservaciones;

  constructor(
    private tecnologiaService: TechnologiesService,
    private interService: InterviewedService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private location: Location,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.getInterviewed();
    this.tecUsadas();
    this.getTecnologias();
    this.fichero = this.test.cv;
    this.caracteresObservaciones = this.test.observaciones;
    console.log(this.test.cv);

  }

  volver(): void {
    this.location.back();
  }

  tecUsadas(): void {
    this.interService.getTechnologiesUsadas().subscribe(techs => this.tecnologiasUsadas = techs);
  }

  getInterviewed(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.interService.getInterviewed(id).subscribe(interV => {
      this.editInterviewed = interV;
      this.test = Object.assign({}, this.editInterviewed);
    });
  }

  getTecnologias(): void {
    this.tecnologias = null;
    this.tecnologiaService.getTechnologies().subscribe(technologies => {
      this.tecnologias = technologies;
      this.tecnologias.forEach(element => {
      });
    });
  }

  saveEdit(): void {
    this.test.tecnologias = this.tecnologiasUsadas;
    this.interService.updateInterviewed(this.test)
      .subscribe(() =>
        this.snackBar.open('Entrevistado modificado correctamente', 'Vale', {
          duration: 2000
        })
      );

  }
  updateTechnologiesUsadas(tech: Technology): void {

    this.interService.updateTechnologiesUsadas(tech).subscribe(techn => this.tecnologiasUsadas = techn);
  }

  archivoSeleccionado(event) {
    this.archivoSelec = event.target.files[0];
    this.test.cv = this.archivoSelec;
  }

  addTecnologia(tecnologia: string): void {
    let idNew = 0;
    this.tecnologias.forEach(element => {
      if (tecnologia === element.name) {
        idNew = element.id;
      }
    }
    );
    const tecnoMarcada = {
      id: idNew,
      name: tecnologia,
    };
    //this.tecnologiasUsadas.push(tecnoMarcada);
  }

  comprobarTecnologia(pTecnologia: string): boolean {
    console.log(pTecnologia);
    this.tecnologiasUsadas.forEach(element => {
      if (element.name === pTecnologia) {
        return true;
      }
    });
    return false;
  }

}
