import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInterviewedComponent } from './edit-interviewed.component';

describe('EditInterviewedComponent', () => {
  let component: EditInterviewedComponent;
  let fixture: ComponentFixture<EditInterviewedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInterviewedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInterviewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
