import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaInterviewedComponent } from './lista-interviewed.component';

describe('ListaInterviewedComponent', () => {
  let component: ListaInterviewedComponent;
  let fixture: ComponentFixture<ListaInterviewedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaInterviewedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaInterviewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
