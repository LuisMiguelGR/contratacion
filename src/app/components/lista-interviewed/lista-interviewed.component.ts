import { Component, OnInit, Inject } from '@angular/core';
import { Interviewed } from '../../interviewed';
import { InterviewedService } from '../../servicios/interviewed.service';
import { MatSnackBar, SimpleSnackBar } from '@angular/material';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-eliminar',
  templateUrl: './modal-eliminar.component.html'
})

export class EliminarComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EliminarComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string) { }

  ngOnInit() {

  }
  cancelar(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'app-lista-interviewed',
  templateUrl: './lista-interviewed.component.html',
  styleUrls: ['./lista-interviewed.component.scss']
})
export class ListaInterviewedComponent implements OnInit {

  inter: any;
  intervieweds = [];
  editInterviewed: Interviewed;


  constructor(
    private interService: InterviewedService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getIntervieweds();
  }

  getIntervieweds(): void {
    this.interService.getIntervieweds().subscribe(intervieweds => {
      this.intervieweds = intervieweds;
    });
  }

  delInterviewed(inter: Interviewed): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open( EliminarComponent , dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('Entrevistado eliminado correctamente', 'X', {
          duration: 800
        });
        this.interService.delInterviewed(inter).subscribe();
      } else {
        return;
      }
    });
  }
}
