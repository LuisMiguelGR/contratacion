import { Injectable } from '@angular/core';
import { technologies } from '../mock-technologies';
import { Observable, of } from 'rxjs';
import { Technology } from '../technology';

@Injectable({
  providedIn: 'root'
})
export class TechnologiesService {

  technologies = new Technology();

  constructor() { }

  getTechnologies(): Observable<Technology[]> {
    return of(technologies);
  }



}