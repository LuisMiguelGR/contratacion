import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../usuario';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private http: HttpClient
  ) { }


  inicioSesion(usuario: Usuario) {
    this.router.navigate(['/perfil']);
  }

  estaOn(): boolean {
    if (localStorage.getItem('usuario') === null) {
      return true;
    } else {
      return false;
    }
  }

  login(usuario: string, password: string) {
    return this.http.post('https://reqres.in/api/login', {
      email: usuario,
      password: password
    });
  }

}