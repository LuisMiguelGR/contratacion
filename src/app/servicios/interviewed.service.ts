import { Injectable } from '@angular/core';
import { intervieweds } from '../mock-interviewed';
import { Observable, of } from 'rxjs';
import { Interviewed } from '../interviewed';
import { Technology } from '../technology';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterviewedService {

  arrayInterviewed = intervieweds;
  tecUsadas: Technology[] = [];
  // tecUsadasNuevo: Technology[] = [];

  constructor() { }

  getIntervieweds(): Observable<Interviewed[]> {
    return of(this.arrayInterviewed);
  }

  getInterviewed(id: number): Observable<Interviewed> {
    this.tecUsadas = intervieweds.find(inter => inter.id === id).tecnologias;
    return of(intervieweds.find(inter => inter.id === id));
  }

  addInterviewed(nuevoInterviewed): Observable<any> {
    nuevoInterviewed.id = this.arrayInterviewed.length + 1;
    this.arrayInterviewed.push(nuevoInterviewed);
    return new Observable<any>(observer => {
      observer.next('Entrevistado añadido correctamente');
    });
  }

  delInterviewed(borrarInterviewed): Observable<any> {
    console.log(borrarInterviewed);
    const position = this.arrayInterviewed.indexOf(borrarInterviewed);
    this.arrayInterviewed.splice(position, 1);
    return new Observable<any>(observer => {
      observer.next('Borrado con exito');
    });
  }

  updateInterviewed(interUpdate: Interviewed): Observable<any> {
    console.dir(interUpdate);
    this.arrayInterviewed[interUpdate.id - 1] = interUpdate;
    console.log(this.arrayInterviewed);
    return new Observable<any>(observer => {
      observer.next('Editado con exito');
    });
  }

  updateStatus(interUpdate: Interviewed): Observable<void> {
    this.arrayInterviewed[interUpdate.id - 1].estado = interUpdate.estado;
    return new Observable<any>(observer => {
      observer.next('Cambiado el estado');
    });
  }

  getTechnologiesUsadas(): Observable<Technology[]> {
    return new Observable<Technology[]>(observer => {
      observer.next(this.tecUsadas);
    });

  }

  updateTechnologiesUsadas(tech: Technology): Observable<Technology[]> {
      if (this.tecUsadas.includes(tech)) {
        this.tecUsadas.splice(this.tecUsadas.indexOf(tech), 1);
      } else {
        this.tecUsadas.push(tech);
    }
      return new Observable<Technology[]>(observer => { observer.next(this.tecUsadas); });
  }
/*
  getCv(): Observable<Blob> {
    const headers = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Accept' : 'application/json'
    });
    return new Observable<Blob>(observer => {
      observer.next()
    });
  }
*/
}
