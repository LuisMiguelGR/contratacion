import { TestBed } from '@angular/core/testing';

import { InterviewedService } from './interviewed.service';

describe('InterviewedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterviewedService = TestBed.get(InterviewedService);
    expect(service).toBeTruthy();
  });
});
