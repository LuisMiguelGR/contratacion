import { Injectable } from '@angular/core';
import { Usuario } from './../usuario';

@Injectable()
export class LogueadoService {

  usuarioLogueado;
  public registrado: Usuario;

  constructor() {
  	this.usuarioLogueado = false;
  }

  setUsuarioLogueado(usuario: Usuario) {
    this.usuarioLogueado = true;
    this.registrado = usuario;
    localStorage.setItem('actual', JSON.stringify(usuario));
  
  }

  getUsuarioLogueado() {
  	return JSON.parse(localStorage.getItem('actual'));
  }

}