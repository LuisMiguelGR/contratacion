import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Imports generales
import { PerfilComponent } from './components/perfil/perfil.component';
import { LoginComponent } from './components/login/login.component';

// Imports entrevistados
import { ListaInterviewedComponent } from './components/lista-interviewed/lista-interviewed.component';
import { AltaEntrevistadoComponent } from './components/alta-entrevistado/alta-entrevistado.component';
import { EditInterviewedComponent } from './components/edit-interviewed/edit-interviewed.component';


const rutas: Routes = [
  { path: 'perfil', component: PerfilComponent, children: [
    { path: '', redirectTo: 'entrevistados', pathMatch: 'full' },
    { path: 'entrevistados', component: ListaInterviewedComponent},
    { path: 'anadir', component: AltaEntrevistadoComponent },
    { path: 'edit/:id', component: EditInterviewedComponent },
  ] },
  { path: '', component: LoginComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
