// Modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { MatInputModule } from '@angular/material/input'; 
//import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

// Servicios
import { InterviewedService } from './servicios/interviewed.service';
import { LoginService } from './servicios/login.service';
import { LogueadoService } from './servicios/logueado.service';


// Todos componentes

// Compartidos
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { PerfilComponent } from './components/perfil/perfil.component';


// Entrevistados
import { AltaEntrevistadoComponent } from './components/alta-entrevistado/alta-entrevistado.component';
import { ListaInterviewedComponent } from './components/lista-interviewed/lista-interviewed.component';
import { EditInterviewedComponent } from './components/edit-interviewed/edit-interviewed.component';
import { EliminarComponent } from './components/lista-interviewed/lista-interviewed.component';

// Angular Material
import {
  MatToolbarModule,
  MatTabsModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatChipsModule,
  MatSnackBarModule,
  MatDatepickerModule,
  MatNativeDateModule,
  NativeDateModule,
  MatSelectModule
} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';

// Animaciones
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

// FireBase
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PerfilComponent,
    AltaEntrevistadoComponent,
    ListaInterviewedComponent,
    EditInterviewedComponent,
    LoginComponent,
    EliminarComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatDialogModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    FormsModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,

    MatInputModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatChipsModule,
    Ng2SearchPipeModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NativeDateModule,
    MatSelectModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule
  ],
  providers: [
    InterviewedService,
    LoginService,
    LogueadoService,
    MatDatepickerModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatInputModule,
    Ng2SearchPipeModule,
    MatSelectModule,
    BrowserAnimationsModule
    
  ],
  bootstrap: [AppComponent],
  entryComponents: [EliminarComponent]

})
export class AppModule { }

