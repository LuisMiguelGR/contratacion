import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../usuario';
import { USUARIOS } from '../mock-usuarios';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class LoginService {

  private usuarios = USUARIOS;
  usuario: string;

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,

  ) {

  }

  private inicioSesion(): void {
    this.router.navigate(['/technologies']);
  }

  validate(user: Usuario): Observable<boolean> {
    this.usuarios.forEach(element => {
      if (user.username === element.username && user.password === element.password) {
        this.inicioSesion();
        return new Observable<boolean>(observer => observer.next(true));
      }
    });
    return new Observable<boolean>(observer => observer.next(false));
  }

  // checkLocalStorage(): Observable<string> {
  //   let usuario;
  //   if (!localStorage.getItem('usuario')) {
  //     localStorage.setItem('usuario', 'Anónimo');
  //     usuario = localStorage.getItem('usuario');
  //   } else {
  //     usuario = localStorage.getItem('usuario');
  //   }
  //   return new Observable<string>(observer => observer.next(usuario));
  // }


}
