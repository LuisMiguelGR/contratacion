import { Usuario } from './usuario';

const user1 = new Usuario('mateo', 'abc123.');
const user2 = new Usuario('admin', 'abc123.');
const user3 = new Usuario('luismi', 'abc123.');

export const USUARIOS: Usuario[] = [user1, user2, user3
];
