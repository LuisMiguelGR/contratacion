import { Technology } from './technology';

export let technologies: Technology[] = [

    { id: 1, name: 'Java', framework : ['Spring', 'Struts', 'Hibernate'], tipo : 'Back-end'},
    { id: 2, name: 'Php', framework : ['Laravel', 'Symfony', 'CakePHP'], tipo : 'Back-end' },
    { id: 3, name: 'JavaScript', framework : ['Angular', 'Vue.js', 'React.js'], tipo : 'Front-end' },
    { id: 4, name: 'CSS', framework : ['Bootstrap', 'Foundation', 'Bulma'], tipo: 'Front-end'},
    { id: 5, name: 'Python', framework : ['Django', 'Pyramid', 'Web2py'], tipo: 'Back-end'},
    { id: 6, name: 'C#', framework : ['.NET', 'Entity'], tipo : 'Back-end' },

  ];

