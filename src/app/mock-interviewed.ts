import { Interviewed } from './interviewed';
import { technologies } from './mock-technologies';
import { DatePipe } from '@angular/common';

export let intervieweds: Interviewed[] = [
    {
        id: 1,
        estado: 'entrevistado',
        nombre: 'Luis Miguel',
        apellidos: 'Garcia Rodriguez',
        dni: '12345678A',
        direccion: 'Carretera Ourense',
        fechaNacimiento: new Date('06/03/1987'),
        email: 'luismiguelgr@gmx.es',
        telefono: 664472297,
        ciudad: 'Monforte',
        provincia: 'Lugo',
        codigoPostal: 27419,
        observaciones: 'Algo',
        // tecnologias: [ {name: 'Java', chequeada: true}, {name: 'Php', chequeada: true}],
        tecnologias: [technologies[1], technologies[2]],
        cv: 'https://firebasestorage.googleapis.com/v0/b/primera-9ff61.appspot.com/o/prueba.txt?alt=media&token=38561a71-b117-40a9-bc96-aa1749e2bbd6',
        entrevistadorAsociado: '52525252C'
    },
    {
        id: 2,
        estado: 'sin entrevistar',
        nombre: 'Mateo',
        apellidos: 'Freije Lopes',
        dni: '987654321A',
        direccion: 'Calle nada',
        fechaNacimiento: new Date('10/10/2000'),
        email: 'mateolt@gmail.es',
        telefono: 658798782,
        ciudad: 'A rua',
        provincia: 'Ourense',
        codigoPostal: 27219,
        observaciones: 'Otro',
        // tecnologias: [{name: 'Java', chequeada: true}],
        tecnologias: [technologies[3]],
        cv: 'https://firebasestorage.googleapis.com/v0/b/primera-9ff61.appspot.com/o/Captura.PNG?alt=media&token=e2423957-8162-4616-9798-e12dbd81a483',
        entrevistadorAsociado: '33333333C'
    }
];

