import { Technology } from './technology';

export class Interviewed {
    id: number;
    estado: string;
    nombre: string;
    apellidos: string;
    dni: string;
    direccion: string;
    fechaNacimiento: Date;
    email: string;
    telefono: number;
    ciudad: string;
    provincia: string;
    codigoPostal: number;
    observaciones: string;
    tecnologias: Technology[];
    cv: string;
    entrevistadorAsociado: string;
}
