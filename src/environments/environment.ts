// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: 'AIzaSyC16l8uo4-jiHH2dBGZGpc4OPHfTTlpmhw',
  firebase: {
    authDomain: 'primera-9ff61.firebaseapp.com',
    databaseURL: 'https://primera-9ff61.firebaseio.com',
    projectId: 'primera-9ff61',
    storageBucket: 'primera-9ff61.appspot.com',
    messagingSenderId: '208514452072'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
